moves = {
    initial: {
        x: null, y: null
    },
    actual: {
        x: null, y: null
    }
};

window.onscroll = () => {

    let content = document.querySelector("#content");
    content.ontouchstart = function (e) {
        moves.initial = {
            x: e.changedTouches[0].clientX,
            y: e.changedTouches[0].clientY
        };
    }
    content.ontouchend = function (e) {
        //Para reestablecer las clases y que se queden en su animación
        if ((moves.actual.y - moves.initial.y) < 100) {
            let headerMobile = document.querySelector("#headerMobile");
            headerMobile.classList.remove("show");
            let footer = document.querySelector("footer");
            footer.classList.remove("show");
        }
        if ((moves.actual.y - moves.initial.y) > 100) {
            let headerMobile = document.querySelector("#headerMobile");
            headerMobile.classList.remove("hide");
            let footer = document.querySelector("footer");
            footer.classList.remove("hide");
        }
    }
    content.ontouchmove = function (e) {
        moves.actual = {
            x: e.changedTouches[0].clientX,
            y: e.changedTouches[0].clientY
        };
        if ((moves.actual.y - moves.initial.y) < 100) {
            //ocultar
            let headerMobile = document.querySelector("#headerMobile");
            headerMobile.classList.add("hide");
            let footer = document.querySelector("footer");
            footer.classList.add("hide");
        }
        if ((moves.actual.y - moves.initial.y) > 100) {
            //aparecer
            let headerMobile = document.querySelector("#headerMobile");
            headerMobile.classList.add("show");
            let footer = document.querySelector("footer");
            footer.classList.add("show");
        }

    }
}

